const readBooks = require('./callback.js');
var readBooksPromise = require('./promise.js')

// soal 2
console.log(">>>>>>>>>>>>>>>>>> soal 2");
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

const eksekusi = (timer , i) => {
  readBooksPromise(timer , books[i])
  .then(e => {
    i = i + 1;
    eksekusi(e , i);
  })
  .catch(e => {})
}

eksekusi(10000 , 0);
