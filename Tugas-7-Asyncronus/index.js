var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000},
]

//soal 1
console.log(">>>>>>>>>>>>>>>>>> soal 1")
// Tulis code untuk memanggil function readBooks di sini
const eksekusi = (timer , i) => {
  readBooks(timer, books[i] , (sisaWaktu) => {
    if (sisaWaktu > 0 && i <= books.length-2){
      i++;
      eksekusi(sisaWaktu , i);
    }
  })
}
eksekusi(10000, 0);
